from flask import Blueprint, render_template, redirect, url_for
from werkzeug.security import check_password_hash, generate_password_hash

from flask_login import current_user, login_user, logout_user, login_required
from blue.models import User

mod = Blueprint('index', __name__, template_folder='templates')

@mod.route('/')
def index():
    if current_user.is_authenticated:
        return redirect(url_for('admin.homepage')) #return 'The current user is '+current_user.username
    else:
        return redirect(url_for('login.login'))
#hash='asdf'
''' 
    hash = generate_password_hash('foobar')
    hash = check_password_hash(hash, 'asdf')
'''
    

@mod.route('/logout')
@login_required
def logout():
    logout_user()
    return 'You are now logged out!'

@mod.route('/home') #@login_required
def home():
    if current_user.is_authenticated:
        return redirect(url_for('admin.homepage')) #return 'The current user is '+current_user.username
    else:
        return 'none'

