from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin
#sentry
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration


app = Flask(__name__)
#sentry
sentry_sdk.init(
    dsn="https://d7fbeb11201642faa5c2bfb5833a60f9@sentry.io/1473272",
    integrations=[FlaskIntegration()]
)

#specify database connection
app.config.from_object('config')
db = SQLAlchemy(app) # this is visible to all child
login_manager = LoginManager()
login_manager.init_app(app)

# database model
from blue.models import *

from .index.routes import mod
from .api.routes import mod
from .site.routes import mod
from .admin.routes import mod
from .login.routes import mod

app.register_blueprint(index.routes.mod)
app.register_blueprint(site.routes.mod)
app.register_blueprint(api.routes.mod, url_prefix='/api')
app.register_blueprint(admin.routes.mod, url_prefix='/admin')
app.register_blueprint(login.routes.mod, url_prefix='/login')

db.create_all()