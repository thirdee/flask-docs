from flask import Blueprint, render_template
from flask_login import current_user, login_user, logout_user, login_required

mod = Blueprint('admin', __name__, template_folder='templates')

@mod.route('/')
@login_required
def homepage():
    return render_template('admin/index.html')