from flask import Blueprint, render_template,redirect,url_for,request
from flask_login import current_user, login_user, logout_user, login_required
from flask_wtf import Form

from app.models import User

from wtforms import StringField, PasswordField
from wtforms.validators import InputRequired

mod = Blueprint('login', __name__, template_folder='templates')

@mod.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('admin.homepage')) #return 'The current user is '+current_user.username
    else:
        form = LoginForm()
        if form.validate_on_submit() and request.method == 'POST':
            username = request.form['username']
            password = request.form['password']        
            user=User.query.filter_by(username=username,password_hash=password).first()
            if user:
                login_user(user)
                return redirect(url_for('admin.homepage'))
            else:
                return render_template('login/index.html', form=form)
        return render_template('login/index.html', form=form)

@mod.route('/logout')
@login_required
def logout():
    logout_user()
    return 'You are now logged out!'

class LoginForm(Form):
    username = StringField('username', validators=[InputRequired()])   
    password = PasswordField('password', validators=[InputRequired()])