from flask import Blueprint, jsonify, request

mod = Blueprint('api', __name__)

languages=[{'name':'javascript'},{'name':'python'},{'name':'ruby'}]

@mod.route('/getstuff')
def getstuff():
    return '{"result": "you are in the API!!!!"}'

@mod.route('/lang',methods=['GET'])
def returnAll():
    return jsonify({'languages':languages})

# READ
@mod.route('/lang/<string:name>',methods=['GET'])
def returnOne(name):
    langs = [language for language in languages if language['name'] == name]
    return jsonify({'language' : langs[0]})

# CREATE
@mod.route('/lang',methods=['POST']) # https://www.youtube.com/watch?v=qH--M56OsUg&list=PLXmMXHVSvS-AFMUmbBeIfL3PqTvgw8ygb&index=2
def addOne(): 
    language = {'name':request.json['name']}
    languages.append(language)
    return jsonify({'languages':languages})

# UPDATE
@mod.route('/lang/<string:name>', methods=['PUT']) # https://www.youtube.com/watch?v=2gunLuqHvc8&list=PLXmMXHVSvS-AFMUmbBeIfL3PqTvgw8ygb&index=3
def editOne(name):
	langs = [language for language in languages if language['name'] == name]
	langs[0]['name'] = request.json['name']
	return jsonify({'language' : langs[0]})    

# DELETE
@mod.route('/lang/<string:name>', methods=['DELETE']) # https://www.youtube.com/watch?v=I64c1L_Zl2Y&list=PLXmMXHVSvS-AFMUmbBeIfL3PqTvgw8ygb&index=4
def removeOne(name):
	lang = [language for language in languages if language['name'] == name]
	languages.remove(lang[0])
	return jsonify({'languages' : languages})    
