from flask import Flask

app = Flask(__name__)

from .index.routes import mod
from .api.routes import mod
from .site.routes import mod
from .admin.routes import mod

app.register_blueprint(index.routes.mod)
app.register_blueprint(site.routes.mod)
app.register_blueprint(api.routes.mod, url_prefix='/api')
app.register_blueprint(admin.routes.mod, url_prefix='/admin')