from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin

app = Flask(__name__)

#specify database connection
app.config.from_object('config')
db = SQLAlchemy(app) # this is visible to all child
#flask-login
login_manager = LoginManager()
login_manager.init_app(app)

# database model
from .models import *

from .index.routes import mod
from .api.routes import mod
from .site.routes import mod
from .admin.routes import mod
from .login.routes import mod

app.register_blueprint(index.routes.mod)
app.register_blueprint(site.routes.mod)
app.register_blueprint(api.routes.mod, url_prefix='/api')
app.register_blueprint(admin.routes.mod, url_prefix='/admin')
app.register_blueprint(login.routes.mod, url_prefix='/auth')

# run database model
db.create_all()