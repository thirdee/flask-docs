# Import flask and template operators
from flask import Flask, render_template,session, blueprints,jsonify,request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from functools import wraps
from flask_wtf.csrf import CSRFProtect,CSRFError

# Define the WSGI application object
app = Flask(__name__, static_url_path = '/static')

# Configurations
app.config.from_object('config')
# Define the database object which is imported
# by modules and controllers
db = SQLAlchemy(app)
from app.models import *

from .api.routes import mod
from .site.routes import mod
from .admin.routes import mod

############################## START OPTIONS
# csrf protection
csrf=CSRFProtect(app)
@app.errorhandler(CSRFError)
def csrf_error(reason):
    # return jsonify(success=True,error=reason)
    return render_template('error.html.j2', error=reason), 400

# HTTP error handling route
@app.errorhandler(404)
def not_found(error):
	return render_template('error.html.j2', error=error) , 404

# authorization for the logged in state
# modify this according to your needs
def requires_auth(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		# FIXME: change this part according to your login mechanism
		if 'user_uid' not in session:
			return jsonify(success=False, message="Unauthorized entry. Login First"), 400
		return f(*args, **kwargs)
	return decorated
############################# END OPTIONS

app.register_blueprint(site.routes.mod)
app.register_blueprint(api.routes.mod, url_prefix='/api')
app.register_blueprint(admin.routes.mod, url_prefix='/admin')

# Build the database:
# This will create the database file using SQLAlchemy
db.create_all()