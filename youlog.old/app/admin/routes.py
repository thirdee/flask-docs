from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for, jsonify
from app import db, models
import app.admin.helper as helper

mod = Blueprint('admin', __name__, template_folder='templates')

@mod.route('/')
def homepage():
    return render_template('admin/index.html')