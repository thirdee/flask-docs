from app import db
## Creating the tables and columns
class Accounts(db.Model): 
    # NOTES: all fields must be filled-in in order to verify the account, all fields can be change except company name and ID
    id                  = db.Column(db.Integer, primary_key=True)
    company_name        = db.Column(db.String(200), index=True, unique=True, nullable=False)
    email               = db.Column(db.String(200), index=True, unique=True, nullable=False) # transferable
    password            = db.Column(db.String(1000), index=True, unique=True, nullable=False)
    firstname           = db.Column(db.String(100), index=True, unique=True, nullable=False) # transferable
    lastname            = db.Column(db.String(100), index=True, unique=True, nullable=False) # transferable
    home_address        = db.Column(db.String(200), index=True, unique=True, nullable=True)
    gender              = db.Column(db.String(100), index=True, unique=True, nullable=True)
    mobile_number       = db.Column(db.String(100), index=True, unique=True, nullable=True) 
    join_date           = db.Column(db.DateTime, index=True, unique=True, nullable=False)
    update_date         = db.Column(db.DateTime, index=True, unique=True, nullable=True)
    #this is for sqlalchemy
    address_list       = db.relationship('Address_list', backref='accounts', lazy='dynamic')
    address_list       = db.relationship('Employee', backref='accounts', lazy='dynamic')

class Employee(db.Model):
    id                  = db.Column(db.Integer, primary_key=True)
    company_id          = db.Column(db.Integer, db.ForeignKey('accounts.id'))
    email               = db.Column(db.String(200), index=True, unique=True, nullable=False)
    password            = db.Column(db.String(1000), index=True, unique=True, nullable=False)
    firstname           = db.Column(db.String(100), index=True, unique=True, nullable=False)
    lastname            = db.Column(db.String(100), index=True, unique=True, nullable=False)
    home_address        = db.Column(db.String(200), index=True, unique=True, nullable=True)
    gender              = db.Column(db.String(100), index=True, unique=True, nullable=True)
    mobile_number       = db.Column(db.String(100), index=True, unique=True, nullable=True)
    join_date           = db.Column(db.DateTime, index=True, unique=True, nullable=True)
    update_date         = db.Column(db.DateTime, index=True, unique=True, nullable=True)    
    #this is for sqlalchemy
    address_list       = db.relationship('Address_list', backref='accounts', lazy='dynamic')    

class Address_list(db.Model):
    id                  = db.Column(db.Integer, primary_key=True)
    # to account
    uid                 = db.Column(db.Integer, db.ForeignKey('accounts.id'))
    company_id          = db.Column(db.Integer, db.ForeignKey('employee.id'))
    src_address         = db.Column(db.String(12), index=False, unique=False, nullable=True)
    date_active         = db.Column(db.DateTime, index=False, unique=False, nullable=True)